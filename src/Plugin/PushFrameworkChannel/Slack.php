<?php

namespace Drupal\pf_slack\Plugin\PushFrameworkChannel;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\push_framework\ChannelBase;
use Drupal\user\UserInterface;
use JoliCode\Slack\ClientFactory;
use Markdownify\Converter;

/**
 * Plugin implementation of the push framework channel.
 *
 * @ChannelPlugin(
 *   id = "slack",
 *   label = @Translation("Slack"),
 *   description = @Translation("Provides the Slack channel plugin.")
 * )
 */
class Slack extends ChannelBase {

  /**
   * {@inheritdoc}
   */
  public function getConfigName(): string {
    return 'pf_slack.settings';
  }

  /**
   * {@inheritdoc}
   */
  public function applicable(UserInterface $user): bool {
    // This channel sends directly, so it never applies to other notifications.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function send(UserInterface $user, ContentEntityInterface $entity, array $content, int $attempt): string {
    $converter = new Converter(Converter::LINK_AFTER_CONTENT, intval(FALSE), FALSE);
    $output = array_shift($content);
    $message = '<h1>' . $output['subject'] . '</h1>' . PHP_EOL . $output['body'];
    $client = ClientFactory::create($this->pluginConfig->get('token'));
    $client->chatPostMessage([
      'username' => $this->pluginConfig->get('username'),
      'channel' => $this->pluginConfig->get('channel_id'),
      'text' => strip_tags($converter->parseString($message)),
    ]);
    return self::RESULT_STATUS_SUCCESS;
  }

}
