<?php

namespace Drupal\pf_slack\Plugin\DanseRecipientSelection;

use Drupal\push_framework\Plugin\DanseRecipientSelection\DirectPush;

/**
 * Plugin implementation of DANSE.
 *
 * @DanseRecipientSelection(
 *   id = "slack",
 *   label = @Translation("Slack"),
 *   description = @Translation("Marks notifications to be sent to Slack only.")
 * )
 */
class Slack extends DirectPush {

  /**
   * {@inheritdoc}
   */
  public function directPushChannelId(): string {
    return 'slack';
  }

}
