<?php

namespace Drupal\pf_slack\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\push_framework\Form\Settings as FrameworkSettings;

/**
 * Configure Push Framework Slack settings.
 */
class Settings extends FrameworkSettings {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'pf_slack_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['pf_slack.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['slack_details'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="active"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['slack_details']['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Token'),
      '#default_value' => $this->pluginConfig->get('token'),
    ];
    $form['slack_details']['channel_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Channel ID'),
      '#default_value' => $this->pluginConfig->get('channel_id'),
    ];
    $form['slack_details']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User name'),
      '#default_value' => $this->pluginConfig->get('username'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->pluginConfig
      ->set('token', $form_state->getValue('token'))
      ->set('channel_id', $form_state->getValue('channel_id'))
      ->set('username', $form_state->getValue('username'));
    parent::submitForm($form, $form_state);
  }

}
